from . import command


def getTemp():
    """Get temperature reading from DHT20."""

    return (command("Getting temperature", "Dt", int))/100


def getHumidity():
    """Get humidity reading from DHT20."""

    return (command("Getting humidity", "Dh", int))/100
