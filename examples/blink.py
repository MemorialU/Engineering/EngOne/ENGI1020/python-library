#!/usr/bin/env python3

from engi1020.arduino import *
from time import sleep

while True:
    digital_write(13, True)
    sleep(0.2)
    digital_write(13, False)
    sleep(0.2)
