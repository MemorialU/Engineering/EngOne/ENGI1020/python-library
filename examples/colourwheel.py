#!/usr/bin/env python3

from engi1020.arduino import *

while True:
    value = analog_read(0)
    hue = value / 1024

    lcd_move_cursor(0, 0)
    lcd_print(f'A0:   {value:10}')
    lcd_move_cursor(1, 0)
    lcd_print(f'Hue:  {hue:10}')

    lcd_hsv(hue, 1, 255)
